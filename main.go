package main

import (
	"fmt"
	"log"
	"time"

	"github.com/mmcdole/gofeed"
	"gopkg.in/mail.v2"
)

var rssList, GUIDList []string
var lastUpdate time.Time

func sendMail(title string, body string) {
	log.Println("Sending")
	m := mail.NewMessage()
	m.SetHeader("From", "z@zhufn.fun")
	m.SetHeader("To", "z@zhufn.fun")
	m.SetHeader("Subject", title)
	m.SetBody("text/html", body)
	d := mail.NewDialer("smtp.yandex.com", 465, "z@zhufn.fun", "lfzuepwtvzkozabi")
	d.StartTLSPolicy = mail.MandatoryStartTLS
	if err := d.DialAndSend(m); err != nil {
		fmt.Println(err)
	}
	log.Println("Sent")
}

func autoUpdate() {
	fmt.Println("Now is ", time.Now())
	for _, url := range rssList {
		fp := gofeed.NewParser()
		feed, err := fp.ParseURL(url)
		if err != nil {
		    fmt.Println(err)
		}
		for _, v := range feed.Items {
			thisTime, err := time.Parse("Mon, 2 Jan 2006 15:04:05 GMT", v.Published)
			if err != nil {
				fmt.Println(err)
				thisTime, err = time.Parse("Mon, 2 Jan 2006 15:04:05 -0700", v.Published)
			} 
			if err != nil {
				fmt.Println(err)
			}
			if err == nil {
				if thisTime.Unix() > lastUpdate.Unix() {
					sendMail("["+feed.Title+"]"+v.Title, v.Description)
				}
			} else {
				break
			}
		}
	}
	lastUpdate = time.Now()

	time.Sleep(time.Second * 60)
	autoUpdate()
}

func main() {
	rssList = []string{"https://www.ithome.com/rss", "https://sspai.com/feed", "https://rss.zhufn.fun/engadget-cn"}
	lastUpdate = time.Now()
	go autoUpdate()
	for {
		fmt.Scanf("")
	}
}
