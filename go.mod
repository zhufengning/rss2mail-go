module rss2mail

go 1.13

require (
	github.com/mmcdole/gofeed v1.0.0
	gopkg.in/alexcesaro/quotedprintable.v3 v3.0.0-20150716171945-2caba252f4dc // indirect
	gopkg.in/mail.v2 v2.3.1
)
